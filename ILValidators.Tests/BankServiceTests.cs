﻿namespace ILValidators.Tests
{
    using ILValidators.Model;
    using ILValidators.Services;
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;

    public class BankServiceTests
    {

        [Fact]
        public async Task ListOfBranchesByBankWillReturnSuccess()
        {
            var current = await Validators.IL.Bank.GetBranchesByBankAsync(31);
            Assert.True(current.Any());
        }

        [Fact]
        public void Get_Banks_Will_Return_List()
        {
            var result = Validators.IL.Bank.GetBanks();
            Assert.True(result.Any());
        }


        [Fact]
        public void Valid_Doar_Account_Nr_Will_Return_Success()
        {
            const string account = "59121900";
            const string branch = "1";
            const int bank = 9;

            var status = Validators.IL.Bank.IsValidAccountNumberAsync(bank, branch, account);
            Assert.True(status == BankValidationStatus.SUCCESS);
        }

        [Fact]
        public void Valid_Leumi_Account_Nr_Will_Return_Success()
        {
            const string account = "11012010083";
            const string branch = "603";
            const int bank = 10;

            var status = Validators.IL.Bank.IsValidAccountNumberAsync(bank, branch, account);
            Assert.True(status == BankValidationStatus.SUCCESS);
        }

        [Fact]
        public void Valid_Discount_Account_Nr_Will_Return_Success()
        {
            const string account = "426334";
            const string branch = "085";
            const int bank = 11;

            var status = Validators.IL.Bank.IsValidAccountNumberAsync(bank, branch, account);
            Assert.True(status == BankValidationStatus.SUCCESS);
        }

        [Fact]
        public void Valid_Poalim_Account_Nr_Will_Return_Success()
        {
            const string account = "506333";
            const string branch = "771";
            const int bank = 12;

            var status = Validators.IL.Bank.IsValidAccountNumberAsync(bank, branch, account);
            Assert.True(status == BankValidationStatus.SUCCESS);
        }

        [Fact]
        public void Valid_Igud_Account_Nr_Will_Return_Success()
        {
            const string account = "11048360022";
            const string branch = "51";
            const int bank = 13;

            var status = Validators.IL.Bank.IsValidAccountNumberAsync(bank, branch, account);
            Assert.True(status == BankValidationStatus.SUCCESS);
        }

        [Fact]
        public void Valid_Otzar_Hahayal_Account_Nr_Will_Return_Success()
        {
            const string account = "409019755";
            const string branch = "316";
            const int bank = 14;

            var status = Validators.IL.Bank.IsValidAccountNumberAsync(bank, branch, account);
            Assert.True(status == BankValidationStatus.SUCCESS);
        }

        [Fact]
        public void Valid_Otzar_Hahayal_Branch_Will_Return_Success()
        {
            const string branch = "316";
            const int bank = 14;

            var current = Validators.IL.Bank.GetBranchDetailsAsync(bank, branch);
            Assert.NotNull(current);
        }


        [Fact]
        public void Valid_Markantil_Account_Nr_Will_Return_Success()
        {
            const string account = "62694520";
            const string branch = "674";
            const int bank = 17;

            var status = Validators.IL.Bank.IsValidAccountNumberAsync(bank, branch, account);
            Assert.True(status == BankValidationStatus.SUCCESS);
        }

        [Fact]
        public void Valid_Mizrahi_Account_Nr_Will_Return_Success()
        {
            const string account = "208199";
            const string branch = "451";
            const int bank = 20;

            var status = Validators.IL.Bank.IsValidAccountNumberAsync(bank, branch, account);
            Assert.True(status == BankValidationStatus.SUCCESS);
        }

        [Fact]
        public void Valid_Citibank_Account_Nr_Will_Return_Success()
        {
            const string account = "700241017";
            const string branch = "2";
            const int bank = 22;

            var status = Validators.IL.Bank.IsValidAccountNumberAsync(bank, branch, account);
            Assert.True(status == BankValidationStatus.SUCCESS);
        }

        [Fact]
        public void Valid_BNP_PARIBAS_Account_Nr_Will_Return_Bank_Not_Supported()
        {
            const string account = "600241010";
            const string branch = "1";
            const int bank = 25;

            var status = Validators.IL.Bank.IsValidAccountNumberAsync(bank, branch, account);
            Assert.True(status == BankValidationStatus.BANK_NOT_SUPPORTED);
        }

        [Fact]
        public void Wrong_Account_Nr_Will_Return_Fail()
        {
            const string account = "110107600";
            const string branch = "821";
            const int bank = 10;

            var status = Validators.IL.Bank.IsValidAccountNumberAsync(bank, branch, account);
            Assert.True(status == BankValidationStatus.FAILED);
        }

        [Fact]
        public void Wrong_Bank_Nr_Will_Return_Bank_not_Supported()
        {
            const string account = "426334";
            const string branch = "085";
            const int bank = 1000;

            var status = Validators.IL.Bank.IsValidAccountNumberAsync(bank, branch, account);
            Assert.True(status == BankValidationStatus.BANK_NOT_SUPPORTED);
        }

        [Fact]
        //[ExpectedException(typeof(ApplicationException))]
        public void Empty_Account_Nr_Will_Return_Empty_Account_Number()
        {
            const string account = "";
            const string branch = "085";
            const int bank = 11;

            var status = Validators.IL.Bank.IsValidAccountNumberAsync(bank, branch, account);
            Assert.True(status == BankValidationStatus.EMPTY_ACCOUNT_NUMBER);
        }

        [Fact]
        //[ExpectedException(typeof(ApplicationException))]
        public void Empty_Branch_Nr_Will_Return_Invalid_branch_Number()
        {
            const string account = "110107600";
            const string branch = "";
            const int bank = 10;

            var status = Validators.IL.Bank.IsValidAccountNumberAsync(bank, branch, account);
            Assert.True(status == BankValidationStatus.INVALID_BRANCH_NUMBER);
        }


        [Fact]
        public void Account_Text_Non_Numeric_Characters_Will_Return_Invalid_Account_Number()
        {
            const string account = "aaaabbbcc";
            const string branch = "085";
            const int bank = 11;

            var status = Validators.IL.Bank.IsValidAccountNumberAsync(bank, branch, account);
            Assert.True(status == BankValidationStatus.INVALID_ACCOUNT_NUMBER);
        }


        #region Row Tests

        /// <summary>
        /// Not exacty a unit test.
        /// This method validates a list of real bank account numbers from a csv file (not included in project).
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task All_Valid_Accounts_Will_Return_Success()
        {
            const string csvName = "valid-accounts-2020.csv";
            const BankValidationStatus validStatus = BankValidationStatus.SUCCESS;
            await AccountRowTest(csvName, validStatus);
        }

        [Fact]
        public async Task All_NonExisting_Branches_Numbers_Will_Return_Branch_Not_Exists()
        {
            const string csvName = "branch-not-exists-accounts.csv";
            const BankValidationStatus validStatus = BankValidationStatus.BRANCH_NOT_EXISTS;
            await AccountRowTest(csvName, validStatus);
        }
        /*
        [Fact]
        public async Task All_Closed_Branches_Account_Numbers_Will_Return_Branch_Closed()
        {
            const string csvName = "branch-closed-accounts.csv";
            const BankValidationStatus validStatus = BankValidationStatus.BRANCH_CLOSED;
            await AccountRowTest(csvName, validStatus);
        }
        */

        
        [Fact]
        public async Task Too_Short_Account_Numbers_Will_Return_Account_Too_Short()
        {
            const string csvName = "accounts-number-too-short.csv";
            const BankValidationStatus validStatus = BankValidationStatus.ACCOUNT_TOO_SHORT;
            await AccountRowTest(csvName, validStatus);
        }

        /// <summary>
        /// Private method to perform all row tests from external data source.
        /// </summary>
        /// <param name="csvName"></param>
        /// <param name="validStatus"></param>
        private async Task AccountRowTest(string csvName, BankValidationStatus validStatus)
        {
            // Get file that contains list of accounts for test
            string[] lines = await File.ReadAllLinesAsync($"F:\\Projects\\net\\BankAccountValidation\\Accounts.Tests\\App_Data\\{csvName}");
            
            var valid = false;
            foreach (var line in lines)
            {
                if (string.IsNullOrWhiteSpace(line))
                {
                    continue;
                }

                var data = line.Split(',');
                var bank = int.Parse(data[0].Trim());
                var branch = (data[1] ?? string.Empty).Trim();
                var account = (data[2] ?? string.Empty).Trim();


                var status = Validators.IL.Bank.IsValidAccountNumberAsync(bank, branch, account);
                valid = status == validStatus;
                if (!valid)
                {
                    Console.WriteLine(string.Concat(status, " ==> ", bank, " ==> ", branch + " ==> ", account));
                }
            }

            // Write result
            Assert.True(valid);
        }

        #endregion RowTests
    }
}