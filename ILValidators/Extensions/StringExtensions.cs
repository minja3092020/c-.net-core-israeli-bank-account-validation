﻿namespace ILValidators.Extensions
{

    #region Using directives

    using System;
    using System.Linq;
    using System.Text.RegularExpressions;

    #endregion Using directives

    internal static class StringExtensions
    {
        /// <summary>
        /// A method to clean the non-numeric chars from string.
        /// The method arg may contain chars like /.-\ or whitespace.
        /// </summary>
        /// <param name="inputStr" type="string">The input string.</param>
        /// <returns>returns string without non numeric chars.</returns>
        public static string ToDigitsOnly(this string? inputStr)
        {
            if (string.IsNullOrEmpty(inputStr))
            {
                return string.Empty;
            }
            var listOChars = inputStr.Where(char.IsNumber).ToList();
            return new string(listOChars.ToArray());

            //return Regex.Replace(inputStr, "[^0-9]", string.Empty);
        }

        public static bool IsValidId(this string s)
        {
            // Validate correct input
            if (!Regex.IsMatch(s, @"^\d{5,9}$"))
            {
                return false;
            }

            // The number is too short - add leading 0000
            s = s.PadLeft(9 - s.Length, '0');

            // Check the id number algoritm - http://halemo.net/info/idcard/
            var sum = 0;
            for (var i = 0; i < 9; i++)
            {
                var incNum = Convert.ToInt32(s[i]);
                incNum *= (i % 2) + 1;
                if (incNum > 9)
                {
                    incNum -= 9;
                }
                sum += incNum;
            }
            return sum % 10 == 0;
        }
    }
}
