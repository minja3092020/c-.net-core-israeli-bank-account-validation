﻿namespace ILValidators.Validators
{

    #region Using directives

    using System;
    using Model;
    
    #endregion Using directives

    /// <summary>
    ///  The City bank 'ConcreteStrategy' class, inherits the <see cref="IValidator"/> interface.
    /// </summary>
    internal class CityBankValidator : IValidator
    {

        #region IValidator Members

        /// <summary>
        /// בדיקת חוקיות מספר חשבון בנק - סיטיבנק
        /// <list type="table">
        /// <listheader>
        /// <term>מס' בנק</term>
        /// <description>שם בנק</description>
        /// </listheader>
        /// <item><term>14</term><description>סיטיבנק</description></item>
        /// <item><term>25</term><description>BNP PARIBAS</description></item>
        /// </list>
        /// </summary>
        /// <param name="account" type="AccountInfo">The account info</param>
        /// <returns>returns <see cref="Status"/> enum</returns>
        public BankValidationStatus IsValidAccountNumber(BankAccount account)
        {
            if (account == null || account.Bank == null || account.Branch == null)
            {
                throw new ArgumentNullException("account");
            }

            // הערה: מס' חשבון בסיטי בנק הינו בן 8 ספרות + ספרת בקורת.
            // משתנים A B C D E F G H - X
            var accountNr = account.AccountNr;
            
            // חובה לוודא שמספר החשבון כולל לא פחות מ-9 ספרות
            if (accountNr / 10000000 == 0)
            {
                return BankValidationStatus.ACCOUNT_TOO_SHORT;
            }

            // ספרת ביקורת - משתנה X
            var checkDigit = accountNr % 10;

            // משתנים A B C D E F G H
            accountNr /= 10;

            var n = 1;
            var sum = 0;
            int dig;
            // Counting from the rightmost digit, and moving left.
            while (n < 7)
            {
                dig = Convert.ToInt32(accountNr % 10);
                sum += (dig * (n + 1));
                accountNr /= 10;
                n++;
            }

            n = 1;
            while (n < 3)
            {
                dig = Convert.ToInt32(accountNr % 10);
                sum += dig * ((n + 1));
                accountNr /= 10;
                n++;
            }

            // חובה לוודא שמוגדר ערך שארית
            if (account.Bank.DivResult.HasValue)
            {
                var result = sum % account.Bank.DivResult.Value;

                // רק בחישוב של בנק BNP PARIBAS
                // כאשר המשלים לשארית הוא 10 ספרת הביקורת חייבת להיות 0
                // כאשר המשלים לשארית הוא 11 ספרת הביקורת חייבת להיות 1
                if (account.Bank.Id == 25 && checkDigit < 2)
                {
                    checkDigit += 10;
                }

                //המשלים של השארית ל 11 - חייב להיות זהה לספרת הביקורת.
                if (account.Bank.DivResult - result == checkDigit)
                {
                    return BankValidationStatus.SUCCESS;
                }
            }

            

            return BankValidationStatus.FAILED;
        }


        #endregion


    }
}
