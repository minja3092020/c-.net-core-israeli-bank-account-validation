﻿namespace ILValidators.Validators
{

    #region Using directives

    using System;
    using System.Linq;
    using Model;
    

    #endregion Using directives

    /// <summary>
    ///  The Leumi group 'ConcreteStrategy' class, inherits the <see cref="IValidator"/> interface.
    /// </summary>
    internal class LeumiValidator : IValidator
    {

        #region IValidator Members

        /// <summary>
        /// בדיקת חוקיות מספר חשבון בנק - קבוצת בל"ל
        /// <list type="table">
        /// <listheader>
        /// <term>מס' בנק</term>
        /// <description>שם בנק</description>
        /// </listheader>
        /// <item><term>10</term><description>בנק לאומי</description></item>
        /// <item><term>13</term><description>בנק איגוד</description></item>
        /// <item><term>34</term><description>בנק ערבי ישראלי</description></item>
        /// </list>
        /// </summary>
        /// <param name="account" type="AccountInfo">The account info</param>
        /// <returns>returns <see cref="Status"/> enum</returns>
        public BankValidationStatus IsValidAccountNumber(BankAccount account)
        {
            if (account == null || account.Bank == null || account.Branch == null)
            {
                throw new ArgumentNullException("account");
            }

            var accountNr = account.AccountNr;

            //  H X  להוסיף כמספר בן שתי ספרות את המשתנים.
            var sum = accountNr % 100;

            // משתנים B C D E F G
            accountNr /= 100;

            int dig;
            var n = 1;
            while (n < 7)
            {
                dig = Convert.ToInt32(accountNr % 10);
                sum += (dig * (n + 1));
                accountNr /= 10;
                n++;
            }

            // משתנים S T U
            var branchNr = account.Branch.BranchId;

            // Counting from the rightmost digit, and moving left.
            while (n < 10)
            {
                dig = branchNr % 10;
                sum += (dig * (n + 1));
                branchNr /= 10;
                n++;
            }

            // מתעלמים מכל הספרות למעט היחידות והעשרות
            var result = Convert.ToInt32(sum % 100);

            // כולל רק עשרות ואחדות, נשאר רק לבדוק האם התוצאה שווה 20|60|70|72|90 result-לאחר ש
            if (account.Bank.ValidResult.Contains(result))
            {
                return BankValidationStatus.SUCCESS;
            }

            // כל שארית אחרת אינה חוקית.
            return BankValidationStatus.FAILED;

        }


        #endregion


    }
}
