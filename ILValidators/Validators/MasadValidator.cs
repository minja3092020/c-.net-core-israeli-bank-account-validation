﻿namespace ILValidators.Validators
{

    #region Using directives

    using Model;
    
    #endregion Using directives

    /// <summary>
    ///  The Masad bank 'ConcreteStrategy' class, inherits the <see cref="IValidator"/> interface.
    /// </summary>
    internal class MasadBankValidator : IValidator
    {

        #region IValidator Members

        /// <summary>
        /// בדיקת חוקיות מספר חשבון בנק - בנק מסד
        /// הבדיקה זהה עבור:
        /// <list type="table">
        /// <listheader>
        /// <term>מס' בנק</term>
        /// <description>שם בנק</description>
        /// </listheader>
        /// <item><term>14</term><description>אוצר החייל</description></item>
        /// <item><term>46</term><description>מסד</description></item>
        /// </list>
        /// </summary>
        /// <param name="account" type="AccountInfo">The account info</param>
        /// <returns>returns <see cref="Status"/> enum</returns>
        public BankValidationStatus IsValidAccountNumber(BankAccount account)
        {

            // בדיקה 1 - זהה לקבוצת הפועלים
            if (new PoalimValidator().IsValidAccountNumber(account) == BankValidationStatus.SUCCESS)
            {
                return BankValidationStatus.SUCCESS;
            }

            // בדיקה 2 - זהה לקבוצת הבינלאומי
            var result = new BenLeumiValidator().IsValidAccountNumber(account);
            if (result == BankValidationStatus.SUCCESS)
            {
                return BankValidationStatus.SUCCESS;
            }

            // כל שארית אחרת אינה חוקית.
            return BankValidationStatus.FAILED;
        }


        #endregion


    }
}
