﻿namespace ILValidators.Services
{
    using ILValidators.Extensions;
    using ILValidators.Model;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class Validators
    {
        public sealed class IL
        {
            public sealed class Bank
            {
                private static readonly BankAccountValidationService bankService = new BankAccountValidationService();
                /*
                public static async Task<IEnumerable<dynamic>> GetBanksAsync()
                {
                    return await bankService.GetBanksAsync();
                }
                */
                public static IEnumerable<dynamic> GetBanks()
                {
                    return bankService.GetBanks();
                }

                public static async Task<IEnumerable<dynamic>> GetBranchesByBankAsync(int bank)
                {
                    return await bankService.GetBranchesByBankAsync((SupportedBanks)bank);
                }
                /*
                public static async Task<BankValidationStatus> IsValidAccountNumberAsync(int bank, string branch, string account)
                {
                    return await bankService.IsValidAccountNumberAsync(bank, branch, account);
                }
                */
                public static BankValidationStatus IsValidAccountNumberAsync(int bank, string branch, string account)
                {
                    return bankService.IsValidAccountNumberAsync(bank, branch, account);
                }

                public static async Task<Branch?> GetBranchDetailsAsync(int bank, string branch)
                {
                    return await bankService.GetBranchDetailsAsync(bank, branch);
                }
            }

            /// <summary>
            /// בדיקת תקינות מס ת.ז. ישראלית
            /// </summary>
            /// <param name="id">The input number to validate.</param>
            /// <returns></returns>
            public static bool IsValidIdentityNumber(string id)
            {
                return id.IsValidId();
            }

            /// <summary>
            /// תקינות מספר ח.פ.
            /// </summary>
            /// <param name="id">The company number</param>
            /// <returns></returns>
            public static bool IsValidCompanyNumber(string id)
            {
                if (string.IsNullOrEmpty(id))
                {
                    return false;
                }

                return id.StartsWith("5") && id.IsValidId();
            }

        }
    }
}
