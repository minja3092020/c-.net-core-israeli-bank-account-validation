﻿namespace ILValidators.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    internal class CSVHelper
    {

        public async Task<List<string[]>> CsvFileTextToList(string? csvFilePath)
        {

            if (string.IsNullOrEmpty(csvFilePath))
            {
                throw new ArgumentNullException("csvFilePath");
            }

            try
            {
                var csv = await this.ReadTextAsync(csvFilePath);
                if (string.IsNullOrEmpty(csv))
                {
                    throw new ArgumentOutOfRangeException("csv");
                }

                var list = new List<string[]>();
                foreach (string line in Regex.Split(csv, Environment.NewLine).ToList().Where(s => !string.IsNullOrEmpty(s)))
                {
                    string[] values = line.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        //Trim values
                        values[i] = values[i].Trim('\"');
                    }

                    list.Add(values);
                }
                return list;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        private async Task<string> ReadTextAsync(string filePath)
        {
            try
            {
                using FileStream sourceStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize: 4096, useAsync: true);
                StringBuilder sb = new StringBuilder();

                byte[] buffer = new byte[0x1000];
                int numRead;
                while ((numRead = await sourceStream.ReadAsync(buffer, 0, buffer.Length)) != 0)
                {
                    string text = Encoding.UTF8.GetString(buffer, 0, numRead);
                    sb.Append(text);
                }

                return sb.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}
