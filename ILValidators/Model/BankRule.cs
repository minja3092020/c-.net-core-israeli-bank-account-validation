﻿namespace ILValidators.Model
{
    using System.Collections.Generic;

    /// <summary>
    /// Bank rule model
    /// </summary>
    internal class BankRule : Bank
    {
        /// <summary>
        /// Gets or sets the div result.
        /// </summary>
        /// <value>
        /// The div result.
        /// </value>
        internal int? DivResult { get; set; }

        /// <summary>
        /// Gets or sets the valid result.
        /// </summary>
        /// <value>
        /// The valid result array.
        /// </value>
        internal List<int> ValidResult { get; set; } = new List<int>();

        /// <summary>
        /// Gets or sets the list of active branches.
        /// </summary>
        /// <value>
        /// The list of active branches.
        /// </value>
        internal List<int> Branches { get; set; } = new List<int>();

        /// <summary>
        /// Gets or sets the list of closed branches.
        /// </summary>
        /// <value>
        /// The list of closed branches.
        /// </value>
        internal List<int>? ClosedBranches { get; set; }


        /// <summary>
        /// Gets or sets the account validator strategy class.
        /// </summary>
        /// <value>
        /// The account validator strategy class.
        /// </value>
        internal IValidator? Validator { get; set; }

        /// <summary>
        /// Gets or sets the valid branch results.
        /// </summary>
        /// <value>
        /// The valid branch results array.
        /// </value>
        internal IDictionary<List<int>, List<int>>? ValidBranchResults { get; set; }

    }
}
