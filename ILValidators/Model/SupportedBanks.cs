﻿using System.ComponentModel.DataAnnotations;

namespace ILValidators.Model
{
    public enum SupportedBanks
    {
        [Display(Name = "בנק יהב לעובדי המדינה בע]\"מ")]
        Yahav = 4,

        [Display(Name = "בנק הדואר")]
        Doar = 9,

        [Display(Name = "בנק לאומי לישראל בע\"מ")]
        Leumi = 10,

        [Display(Name = "בנק דיסקונט לישראל בע\"מ")]
        Discount = 11,

        [Display(Name = "בנק הפועלים בע\"מ")]
        Poalim = 12,

        [Display(Name = "בנק אגוד לישראל בע\"מ")]
        Iggud = 13,

        [Display(Name = "בנק אוצר החייל בע\"מ")]
        OzarHahayal = 14,

        [Display(Name = "בנק מרכנתיל דיסקונט בע\"מ")]
        MarcantilDiscount = 17,

        [Display(Name = "בנק מזרחי טפחות בע\"מ")]
        Mizrahi = 20,

        [Display(Name = "Citibank N.A")]
        CityBank = 22,

        [Display(Name = "בנק הבינלאומי הראשון לישראל בע\"מ")]
        BenLeumi = 31,

        [Display(Name = "בנק מסד בע\"מ")]
        Masad = 46,

        [Display(Name = "בנק פועלי אגודת ישראל בע\"מ")]
        AgudatIsrael = 52

    }
}
