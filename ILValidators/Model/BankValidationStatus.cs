﻿using System.ComponentModel.DataAnnotations;

namespace ILValidators.Model
{
    /// <summary>
    /// Enum represents the validation error codes.
    /// </summary>
    public enum BankValidationStatus
    {
        [Display(Name = "הבדיקה הצליחה")]
        SUCCESS = 0,

        [Display(Name = "הבדיקה נכשלה")]
        FAILED = -1,

        [Display(Name = "בנק לא קיים ברשימה")]
        BANK_NOT_SUPPORTED = -2,

        [Display(Name = "מספר חשבון ריק")]
        EMPTY_ACCOUNT_NUMBER = -100,

        [Display(Name = "מספר חשבון לא תקין")]
        INVALID_ACCOUNT_NUMBER = -101,

        [Display(Name = "מספר חשבון ארוך מדי")]
        ACCOUNT_TOO_LONG = -102,

        [Display(Name = "מספר חשבון קצר מדי")]
        ACCOUNT_TOO_SHORT = -103,

        [Display(Name = "מספר סניף לא חוקי")]
        INVALID_BRANCH_NUMBER = -110,


        [Display(Name = "מספר סניף לא קיים ברשימה")]
        BRANCH_NOT_EXISTS = -111,

        [Display(Name = "סניף הבנק נסגר")]
        BRANCH_CLOSED = -112,

       

    }
}