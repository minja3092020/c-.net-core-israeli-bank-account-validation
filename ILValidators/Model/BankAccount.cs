﻿namespace ILValidators.Model
{
    /// <summary>
    /// Account Info model
    /// </summary>
    internal class BankAccount
    {

        /// <summary>
        /// Gets or sets the Bank.
        /// </summary>
        /// <value>
        /// The bank object.
        /// </value>
        public BankRule? Bank { get; set; }

        /// <summary>
        /// Gets or sets the branch details of the of the account holding branch.
        /// </summary>
        /// <value>
        /// The branch object.
        /// </value>
        public Branch? Branch { get; set; }

        /// <summary>
        /// Gets or sets the account number.
        /// </summary>
        /// <value>
        /// The account number.
        /// </value>
        public long AccountNr { get; set; }


    }
}