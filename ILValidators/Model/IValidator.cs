namespace ILValidators.Model
{
    /// <summary>
    /// Strategy interface - different algorithms are represented as Concrete Strategy classes (Validators) and inherit the <see cref="IValidator"/> interface
    /// </summary>
    internal interface IValidator
    {

        /// <summary>
        /// Determines whether the account number is a valid account info.
        /// </summary>
        /// <param name="account">The account info.</param>
        /// <returns>The validation status.</returns>
        BankValidationStatus IsValidAccountNumber(BankAccount account);
    }
}
